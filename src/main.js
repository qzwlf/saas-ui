import Vue from 'vue'
import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './styles/element-variables.scss'
import '@/styles/index.scss' // global css

import App from './App'
import router from './router/routers'
import i18n from './lang' // internationalization
import permission from './components/permission'
import VModal from 'vue-js-modal'

import dict from './components/Dict'
import DataGridTable from './components/DataGridTable/index'
import CustomDialog from './components/CustomDialog/CustomDialog'
import DataGridColumn from './components/DataGridTable/DataGridColumn'
import DataGridColumnSlot from './components/DataGridTable/DataGridColumnSlot'
import DataGridOpts from './components/DataGridOpts/index'
import DestoryOpt from './components/DataGridOpts/DestoryOpt'

import store from './store'

import '@/icons' // icon
import './router/index' // permission control
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})
// Vue.use(mavonEditor)
Vue.use(VModal, {
  dialog: true,
  dynamic: true,
  injectModalsContainer: true,
  dynamicDefaults: {
    clickToClose: false
  }
})

Vue.component('DataGridTable', DataGridTable)
Vue.component('DataGridColumn', DataGridColumn)
Vue.component('DataGridColumnSlot', DataGridColumnSlot)
Vue.component('CustomDialog', CustomDialog)
Vue.component('DataGridOpts', DataGridOpts)
Vue.component('DestoryOpt', DestoryOpt)

Vue.use(permission)
Vue.use(dict)

// Vue.use(ElementUI, { locale })
Vue.config.productionTip = false
// require('babel-polyfill')

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
